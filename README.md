## User API

### Endpoints


**POST** { return token } https://park-it-sam-api.herokuapp.com/user?email=xxx&forename=xxx&surname=xxx

**GET** https://park-it-sam-api.herokuapp.com/authenticate?token=xxx

**GET** https://park-it-sam-api.herokuapp.com/user?token=xxx

**GET** https://park-it-sam-api.herokuapp.com/user/:id?token=xxx

**PUT** https://park-it-sam-api.herokuapp.com/user/:id?token=xxx

**DELETE** https://park-it-sam-api.herokuapp.com/user/:id?token=xxx
