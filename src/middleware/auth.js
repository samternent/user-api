var jwt = require('jsonwebtoken');

module.exports = {
    auth: (req, res, next) => {
        var token = req.query.token;
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, 'secret', function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

        } else {
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    },

    decode: (req, res) => {
        var token = req.query.token;
        var decoded = jwt.decode(token);
        res.json(decoded)
    }
}