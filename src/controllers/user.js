var User = require('../models/user')
var jwt = require('jsonwebtoken');

module.exports = {

    get: (req, res, next) => {
        if (req.params.id)
            var query = User.findById(req.params.id)
        else
            var query = User.find()
        
        query.exec((err, user) => {
            if (err)
                return res.status(500).json({
                    message: 'ERROR'
                })
            if (user) {
                return res.status(200).json({
                    message: 'SUCCESS',
                    user: user
                })
            }

            return res.status(404).json({
                message: `No User With ID ${req.params.id}`
            })
        })
    },

    post: (req, res, next) => {
        var email = req.query.email;
        var forename = req.query.forename;
        var surname = req.query.surname;

        if (!email) {
            return res.status(422).json({
                message:'Missing email'
            });
        }
        if (!forename) {
            return res.status(422).json({
                message:'Missing forename'
            });
        }
        if (!surname) {
            return res.status(422).json({
                message:'Missing surname'
            });
        }

        User.findOne({email: email})
        .exec((err, user) => {
            if (user) {
                return res.status(422).json({
                    message:'Email already exists'
                });
            }

            var user = new User({
                email: email,
                forename: forename,
                surname: surname,
                created: Date.now()
            })

            user.save((err, user) => {
                if (err) {
                    return res.status(500).json({
                        message: 'ERROR'
                    })
                }

                var token = jwt.sign(user, 'secret', {});

                return res.status(200).json({
                    message: 'SUCCESS',
                    token,
                    user
                });
            })
        })
    },

    
    put: (req, res, next) => {
        if (!req.params.id)
            return res.status(422).json({
                message: 'Requires ID'
            })
        
        var {email, forename, surname} = req.query
        var updates = {}

        if (email) {
            updates.email = email
        }
        if (forename) {
            updates.forename = forename
        }
        if (surname) {
            updates.surname = surname
        }

        User.findOneAndUpdate({ _id: req.params.id }, { $set: updates })
        .exec((err, user) => {
            if (err)
                return res.status(500).json({
                    message: 'ERROR'
                })
            
            if (user) {
                return res.status(200).json({
                    message: 'SUCCESS',
                    user: Object.assign(user, updates)
                })
            }

            return res.status(404).json({
                message: `No User With ID ${req.params.id}`
            })
        })
    },
    
    delete: (req, res, next) => {
        if (!req.params.id)
            return res.status(422).json({
                message: 'Requires ID'
            })

        User.findOneAndRemove({ _id: req.params.id })
        .exec((err, user) => {
            if (err)
                return res.status(500).json({
                    message: 'ERROR'
                })
            
            if (user) {
                return res.status(200).json({
                    message: 'SUCCESS',
                    user
                })
            }
            return res.status(404).json({
                message: `No User With ID ${req.params.id}`
            })
        })
    },
}