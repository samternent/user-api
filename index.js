var express = require('express');
var secrets = require('./config/secrets')

var port = process.env.PORT || 3000
var app = express();

app.listen(port, function() {
  console.log('API running on port: ' + port);
});

// Middleware for CORS
const app_url = secrets.app_url

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', app_url);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

require('./src/routes')(app);