// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var connection = require('../../config/connection');

autoIncrement.initialize(connection);

var userSchema = new Schema({ 
    email: { type : String, default: '' },
    forename: { type : String, default: '' },
    surname: { type : String, default: '' },
    created: { type : Date, default: Date.now }
});

userSchema.plugin(autoIncrement.plugin, 'User');

module.exports = connection.model('User', userSchema);
