var mongoose    = require('mongoose');
var secrets = require('./secrets')
var connection = mongoose.connect(secrets.db);

module.exports = connection;