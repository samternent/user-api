var user = require('../controllers/user')
var {auth, decode} = require('../middleware/auth')

module.exports = function (app) {
    app.post('/user', user.post);
    app.get('/authenticate', decode);
    app.get('/user', auth, user.get);
    app.get('/user/:id', auth, user.get);
    app.put('/user/:id', auth, user.put);
    app.delete('/user/:id', auth, user.delete);
}