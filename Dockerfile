FROM node:argon
RUN mkdir /user-api
WORKDIR /user-api
COPY package.json /user-api
RUN npm install
COPY . /user-api
EXPOSE 3000
CMD ["npm", "start"]
